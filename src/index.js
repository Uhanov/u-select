import Select from './Select.vue'

const uSelect = {
  install (Vue, options) {
    Vue.component('u-select', Select)
  }
}

// if Vue is the window object, auto install it
if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(Select)
}

export default uSelect
