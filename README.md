## u-select

> vue.js select plugin

### Installing

npm install u-select --save-dev

### Usage

```js
import uSelect from 'u-select'

Vue.use(uSelect)
```

### Usage
```html
selectData: [
  {key: 1, value: 'Option 1'},
  {key: 2, value: 'Option 2'},
  {key: 3, value: 'Option 3'},
  {key: 4, value: 'Option 4'},
  {key: 5, value: 'Option 5'},
  {key: 6, value: 'Option 6'}
]

<u-select
  :options="selectData"
  :optionValue="'value'"
  :dropdownClass="['someclass']"
  :itemClass="['someclass']"
  :linkClass="['someclass']"
  :selectedItem="0"
  v-on:selected="selectedValue = $event.key"
></u-select>
```

### Options
| Options          |                                                      |
|:-----------------|:-----------------------------------------------------|
| options          | `array of all values`                                |
| popperOptions    | `options for popper.js`                              |
| selectedItem     | `index of default selected item`                     |
| optionValue      | `key name which will be used to show in input`       |
| disabled         | `is input disabled`                                  |
